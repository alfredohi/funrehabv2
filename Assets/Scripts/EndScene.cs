using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class EndScene : MonoBehaviour
{

    public void FinallyQuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void ReplayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    private void OnGUI()
    {
        GUI.skin.box.fontSize = 20;
        //GUI.color = Color.;
        GUI.color = new Color(251, 206, 177, 1.0f); //0.5 is half opacity 
        GUI.Box(new Rect(Screen.width/2-200, Screen.height/2-80, 200, 100), "FINAL SCORE: " + GameManager.score + "\n\nBalls Hit: " + GameManager.total_hits
            + "\nBalls Missed: " + GameManager.total_miss + "\n\nNumber of Balls: " + GameManager.balls); // + "\nBombs Hit: " + GameManager.total_fail
            //+ "\n\nNumber of Balls: " + GameManager.balls + "\nNumber of Bombs: " + GameManager.bombs);
    }

}
