using System.Collections;
using UnityEngine;

public class DetectionPlate : MonoBehaviour
{
    private Vector3 newPlatePosition;
    public Collider spawnArea;

    public GameObject[] fruitPrefabs;


    private void Awake()
    {
        spawnArea = GetComponent<Collider>();
        Debug.Log(spawnArea);
        //spawnArea. = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine").transform.position.z;

    }

    private void Update()
    {
        newPlatePosition = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine").transform.position - new Vector3(0f, 30f, 0);
        GameObject.Find("DetectionPlate").transform.position = newPlatePosition;
        
        
    }

    private void OnTriggerEnter(Collider other)
    {
        ////Check for a match with the specified name on any GameObject that collides with your GameObject
        //if (collision.gameObject.name == "Kiwi(Clone)")
        //{
        //    //If the GameObject's name matches the one you suggest, output this message in the console
        //    Debug.Log("Kiwi");
        //}

        //if (collision.gameObject.name == "Orange(Clone)")
        //{
        //    //If the GameObject's name matches the one you suggest, output this message in the console
        //    Debug.Log("Orange");
        //}
        //if (collision.gameObject.name == "Watermelon(Clone)")
        //{
        //    //If the GameObject's name matches the one you suggest, output this message in the console
        //    Debug.Log("Watermelon");
        //}

        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (other.CompareTag("HelpObject"))
        {
            //If the GameObject has the same tag as specified, output this message in the console
            Debug.Log("Do something else here");
            FindObjectOfType<GameManager>().FailureScore();
        }
    }

    //private void OnEnable()
    //{
    //    StartCoroutine(Spawn());
    //}

    //private void OnDisable()
    //{
    //    StopAllCoroutines();
    //}

    //void OnCollisionEnter(Collision3D collision)
    //{
    //    //Do whatever else you need to do here
    //    collisionCount += 1;
    //}

}