using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public class GameManager : MonoBehaviour
{
    public Material sky;

    public Text scoreText;
    public static float score = 0;
    public static int total_hits = 0;
    public static int total_miss = 0;
    public static int total_fail = 0;
    public static int balls = 0;
    public static int bombs = 0;
    private int failureScore = 10;
    private int Health = 10;
    public static float gamedelay = 5f;
    public static float gametime = 60f;

    private static string Hand_Pos = "(0,0,0)";
    private static string Elbow_Ang = "180";
    private static string Shoulder_Ang = "90";
    private static string time;
    private static string Trunk = "0";
    private string filePath = "./__Test_Angles_Positions__.txt";

    public static double maxJointAngleRight;
    public static double maxJointAngleLeft;

    public static double minJointAngleRight = 180;
    public static double minJointAngleLeft = 180;

    double currentJointAngleShoulder;
    double currentJointAngleElbow;
    double currentJointAngleTrunk;

    public void IncreaseScore()
    {
        score+=1f;
        total_hits++;      
    }

    public void IncreaseScoreY()
    {
        score += 0.5f;
        total_hits++;
    }

    public void IncreaseScoreR()
    {
        score += 0.1f;
        total_hits++;
    }

    public void ReduceScore()
    {
        score--;
        total_fail++;
    }

    public void Ball()
    {
        balls++;
    }

    public void Bomb()
    {
        bombs++;
    }

    public void FailureScore()

    {
       failureScore--;
        Health -= failureScore;
        if (failureScore == 0)
        {
           SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    void Awake()
    {
        if (!File.Exists(filePath))
        {
            File.Create(filePath);
        }
        //File.WriteAllText(filePath, string.Empty);
    }

    // Start is called before the first frame update
    void Start()
    {
        RenderSettings.skybox = sky;
        
        Debug.LogWarning(Settings.armSide);
        GameObject theAvatar = GameObject.Find("Avatar(Clone)");
        Join_Pos jointPos = theAvatar.GetComponent<Join_Pos>();
    }

    // Update is called once per frame
    void Update()
    {
        gamedelay -= Time.deltaTime;
        gametime -= Time.deltaTime;
        if (gametime<=0)
        {
            total_miss = balls - total_hits;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        GameObject theAvatar = GameObject.Find("Avatar(Clone)");
        Join_Pos jointPos = theAvatar.GetComponent<Join_Pos>();
        currentJointAngleTrunk = jointPos.TrunkJointAngle;
        //Trunk = "Trunk";
        Trunk = currentJointAngleTrunk.ToString();
        if(Settings.armSide==0)
        {
            currentJointAngleElbow = jointPos.LeftElbowJointAngle;
            currentJointAngleShoulder = jointPos.LeftShoulderJointAngle;
            Elbow_Ang = currentJointAngleElbow.ToString();
            Shoulder_Ang = currentJointAngleShoulder.ToString();
        }
        else
        {
            currentJointAngleElbow = jointPos.RightElbowJointAngle;
            currentJointAngleShoulder = jointPos.RightShoulderJointAngle;
            Elbow_Ang = currentJointAngleElbow.ToString();
            Shoulder_Ang = currentJointAngleShoulder.ToString();
        }
        //Shoulder_Ang = "Shoulder";
        //Elbow_Ang = "Elbow";

        Vector3 Hand = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm/Character1_RightHand").transform.position;
        Hand_Pos = Hand.ToString();
        //Hand_Pos = "Hand";

        float instant = Time.time;
        time = instant.ToString();

        string text = Hand_Pos + "," + Trunk + "," + Elbow_Ang + "," + Shoulder_Ang + "," + time + "\n";
        using (StreamWriter writer = File.AppendText(filePath))
        {
            writer.WriteLine(text);
        }

    }

    private void OnGUI()
    {   
        if(gamedelay>1){
            GUI.skin.box.fontSize = 40;
            GUI.color = Color.white;
            GUI.skin.box.alignment = TextAnchor.MiddleCenter;
            GUI.Box(new Rect(Screen.width/2-50, Screen.height/2-25, 150, 75), Math.Round(gamedelay)+"...");
        }
        else if(gamedelay>0){
            GUI.skin.box.fontSize = 40;
            GUI.color = Color.white;
            GUI.skin.box.alignment = TextAnchor.MiddleCenter;
            GUI.Box(new Rect(Screen.width/2-50, Screen.height/2-25, 150, 75), "Start!");
        }
        else{
            GUI.skin.box.fontSize = 25;
            GUI.color = Color.white;
            GUI.skin.box.alignment = TextAnchor.MiddleLeft;
            GUI.Box(new Rect(5, 5, 320, 80), "Score: " + score+ "\nTime Remaining: " + Math.Round(gametime) +"s");
        }
    }
}
