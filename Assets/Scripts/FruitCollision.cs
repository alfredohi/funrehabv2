using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitCollision : MonoBehaviour
{
    GameObject frucht;
    AudioSource audiosource;
    public Material fruitMaterial;

    public float LifeSpan;// = Random.Range(8f, 10f);
    public GameObject fruit;
    public float StartTime;


    private Collider fruitCollider;
    //private Renderer surface;
    // Start is called before the first frame update

    void Awake()
    {
        LifeSpan = Random.Range(2.5f, 5f);
        //fruit = GameObject.Find("gameObject");
        FindObjectOfType<GameManager>().Ball();
        Destroy(gameObject, LifeSpan);
    }

    void Start()
    {
        fruitCollider = GetComponent<Collider>();
        //surface = GetComponent<MeshRenderer>();
        audiosource = GetComponent<AudioSource>();
        StartTime = Time.time;
        //fruitMaterial = GetComponent<Apple_Outside>();

        // Create a material with transparent diffuse shader
        fruitMaterial = new Material(Shader.Find("Universal Render Pipeline/Lit"));
        fruitMaterial.color = Color.green;

        // assign the material to the renderer
        GetComponent<Renderer>().material = fruitMaterial;
        gameObject.transform.localScale = new Vector3(0.1f,0.1f,0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Time.time-StartTime > LifeSpan*3f/6f & fruitMaterial.color == Color.green)
        {
            fruitMaterial = new Material(Shader.Find("Universal Render Pipeline/Lit"));
            fruitMaterial.color = Color.yellow;
            GetComponent<Renderer>().material = fruitMaterial;
            //gameObject.transform.localScale = new Vector3(0.20f,0.20f,0.20f);
        } 
        if (Time.time-StartTime > LifeSpan*5f/6f & fruitMaterial.color == Color.yellow)
        {
            fruitMaterial = new Material(Shader.Find("Universal Render Pipeline/Lit"));
            fruitMaterial.color = Color.red;
            GetComponent<Renderer>().material = fruitMaterial;
            //gameObject.transform.localScale = new Vector3(0.20f,0.20f,0.20f);
        } 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") & fruitMaterial.color == Color.green)
        {
            FindObjectOfType<GameManager>().IncreaseScore();
            audiosource.Play();
            //fruitCollider.enabled = false;
            //surface.enabled=false;
            Destroy(gameObject);
        }
        if (other.CompareTag("Player") & fruitMaterial.color == Color.yellow)
        {
            FindObjectOfType<GameManager>().IncreaseScoreY();
            audiosource.Play();
            //fruitCollider.enabled = false;
            //surface.enabled=false;
            Destroy(gameObject);
        }
        if (other.CompareTag("Player") & fruitMaterial.color == Color.red)
        {
            FindObjectOfType<GameManager>().IncreaseScoreR();
            audiosource.Play();
            //fruitCollider.enabled = false;
            //surface.enabled=false;
            Destroy(gameObject);
        }
    }
}
