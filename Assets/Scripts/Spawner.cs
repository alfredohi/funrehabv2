using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Vector3 Shoulder_Midpoint; //= new Vector3(0.07000031f,0.1190407f,-0.05289974f);
    public Vector3 ArmLength; //= new Vector3(1.5f,1.5f,1.5f);
    
    private Vector3 newSpawnerPosition;

    public Collider spawnArea;
    public CapsuleCollider avatarCollider;

    private Vector3[] ballsPositions;
    public GameObject[] fruitPrefabs;
    public GameObject spawn_;

    public float minSpawnDelay;
    public float maxSpawnDelay;
    public float xp;
    public float xn;
    public float yp;
    public float yn;
    public float zp;
    public float zn;


    private void Awake()
    {
        spawnArea = GetComponent<Collider>();
        spawn_ = GameObject.Find("Spawner");
        Shoulder_Midpoint = new Vector3(0.07000031f,0.1190407f,-0.05289974f);
        
        spawn_.transform.position = Shoulder_Midpoint;
    
        Debug.LogWarning(Shoulder_Midpoint);
        //ArmLength = new Vector3(1f,1f,1f);
        //spawn_.transform.localScale = ArmLength;
        if(Settings.difficulty == 2)
        {
            minSpawnDelay = 0.05f;
            maxSpawnDelay = 1.5f;
        }
        else if(Settings.difficulty == 1)
        {
            minSpawnDelay = 1.5f;
            maxSpawnDelay = 3.5f;        
        }
        else
        {
            minSpawnDelay = 3.5f;
            maxSpawnDelay = 7.5f;
        }

        if(Settings.armSide == 0)
        {
            xp = 0.10f;
            xn = -0.55f;
            yp = 0.55f;
            yn = -0.35f;
            zp = 0.00f;
            zn = -0.55f;
        }
        else
        {
            xp = 0.55f;
            xn = -0.10f;
            yp = 0.55f;
            yn = -0.35f;
            zp = 0.00f;
            zn = -0.55f;
        }
    }

    private void Start()
    {
    }

    private void Update()
    {
        Calc_Aux();

        avatarCollider =  GameObject.Find("Avatar(Clone)").GetComponent<CapsuleCollider>();
        //Debug.LogWarning(Shoulder_Midpoint);
        spawn_.transform.position = Shoulder_Midpoint;
    }

    private void OnEnable()
    {
        StartCoroutine(Spawn());
    }

    public void Calc_Aux()
    {
        if(Settings.armSide == 1)
        {
            Shoulder_Midpoint = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm").transform.position;
        }
        else{
            Shoulder_Midpoint = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm").transform.position;
        }
    }

    public float Square(float value)
    {
        return value * value;
    }

    public float Distance(Vector3 from, Vector3 to)
    {
        return Mathf.Sqrt(
            Square(from.x - to.x) +
            Square(from.y - to.y) +
            Square(from.z - to.z)
        );
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public IEnumerator Spawn()
    {
        yield return new WaitForSeconds(7f);
        while (enabled)
        {
            int typ = Random.Range(0, 1);
            if(typ == 0)
                {
                GameObject prefab = fruitPrefabs[0];//Random.Range(0, fruitPrefabs.Length)

                //fazer lista com estos extremos
                Vector3 position = new Vector3();
                position.x = Random.Range(spawnArea.bounds.center.x + xn, spawnArea.bounds.center.x + xp);
                position.y = Random.Range(spawnArea.bounds.center.y + yn, spawnArea.bounds.center.y + yp);
                position.z = Random.Range(spawnArea.bounds.center.z + zn, spawnArea.bounds.center.z + zp);
                
                while ((position.x > avatarCollider.center.x - avatarCollider.radius && position.x < avatarCollider.center.x + avatarCollider.radius) &&
                    (position.y >  avatarCollider.center.y - avatarCollider.height/2 && position.y < avatarCollider.center.y + avatarCollider.height/2) &&
                    (position.z >  avatarCollider.center.z - avatarCollider.radius && position.z < avatarCollider.center.z + avatarCollider.radius))
                {
                    position.x = Random.Range(spawnArea.bounds.center.x + xn, spawnArea.bounds.center.x + xp);
                    position.y = Random.Range(spawnArea.bounds.center.y + yn, spawnArea.bounds.center.y + yp);
                    position.z = Random.Range(spawnArea.bounds.center.z + zn, spawnArea.bounds.center.z + zp);
                    Debug.LogWarning(avatarCollider.bounds);
                    Debug.LogWarning(position);
                }

                Quaternion rotation = Quaternion.Euler(0f, 0f, 0f);
                GameObject fruit = Instantiate(prefab, position, rotation);
            }
            //if (typ == 1)
            //{
            //    GameObject prefab = fruitPrefabs[1];//Random.Range(0, fruitPrefabs.Length)

            //    Vector3 position = new Vector3();
            //    position.x = Random.Range(spawnArea.bounds.center.x - 0.65f, spawnArea.bounds.center.x + 0.65f);
            //    position.y = Random.Range(spawnArea.bounds.center.y - 0.45f, spawnArea.bounds.center.y + 0.65f);
            //    position.z = Random.Range(spawnArea.bounds.center.z - 0.65f, spawnArea.bounds.center.z);

            //    Quaternion rotation = Quaternion.Euler(0f, 0f, 0f);
            //    GameObject bomb = Instantiate(prefab, position, rotation);
            //}
            yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));
        }
    }
   
}