using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Set_1()
    {
        FindObjectOfType<Settings>().S1();
    }

    public void Set_2()
    {
        FindObjectOfType<Settings>().S2();
    }

    public void Set_3()
    {
        FindObjectOfType<Settings>().S3();
    }

    public void Set_L()
    {
        FindObjectOfType<Settings>().L();
    }

    public void Set_R()
    {
        FindObjectOfType<Settings>().R();
    }

    public void ShowHistory()
    {
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

}
