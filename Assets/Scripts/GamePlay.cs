using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlay : MonoBehaviour
{
    public int Score;

    // Update is called once per frame
    void Update()
    {
        Score ++;
        Debug.Log("Score: " + Score);
        if (Score == 2000)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}
