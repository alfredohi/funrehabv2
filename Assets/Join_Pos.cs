using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Join_Pos : MonoBehaviour
{
    Vector3 PosLeftClavical;
    Vector3 PosLeftShoulder;
    Vector3 PosLeftWrist;
    Vector3 PosLeftElbow;

    Vector3 PosRightClavical;
    Vector3 PosRightShoulder;
    Vector3 PosRightWrist;
    Vector3 PosRightElbow;

    Vector3 PosHip;
    Vector3 PosAbdomen;
    Vector3 PosChest;

    Vector3 RightShoulderTrunk;
    Vector3 RightShoulderElbow;
    Vector3 RightElbowShoulder;
    Vector3 RightElbowWrist;

    Vector3 LeftShoulderTrunk;
    Vector3 LeftShoulderElbow;
    Vector3 LeftElbowWrist;
    Vector3 LeftElbowShoulder;

    Vector3 AbdomenChest;
    Vector3 AbdomenHips;

    public double LeftElbowJointAngle;
    public double LeftShoulderJointAngle;
    public double RightElbowJointAngle;
    public double RightShoulderJointAngle;
    public double TrunkJointAngle;

    //double maxJointAngleRight = 0f;
    //double maxJointAngleLeft = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Body
        PosHip = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips").transform.position;
        PosAbdomen = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1").transform.position;
        PosChest = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/").transform.position;

        //Left Arm
        PosLeftWrist = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/" +
            "Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm/Character1_LeftHand").transform.position;

        PosLeftClavical = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder").transform.position;

        PosLeftElbow = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm").transform.position;

        PosLeftShoulder = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm").transform.position;


        //Right Arm
        PosRightWrist = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/" +
            "Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm/Character1_RightHand").transform.position;

        PosRightClavical = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder").transform.position;

        PosRightElbow = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm").transform.position;

        PosRightShoulder = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm").transform.position;

        //Calculate Vectors
        RightElbowWrist = PosRightWrist - PosRightElbow;
        RightElbowShoulder = PosRightShoulder - PosRightElbow;
        //RightElbowJointAngle = 180 - Vector3.Angle(RightElbowWrist, RightElbowShoulder);
        RightElbowJointAngle = Vector3.Angle(RightElbowShoulder, RightElbowWrist);

        RightShoulderElbow = PosRightElbow - PosRightShoulder;
        RightShoulderTrunk = PosRightClavical - PosRightShoulder;
        //RightShoulderJointAngle = 180 - Vector3.Angle(RightShoulderElbow, RightShoulderTrunk);
        RightShoulderJointAngle = Vector3.Angle(RightShoulderTrunk, RightShoulderElbow);

        LeftElbowWrist = PosLeftWrist - PosLeftElbow;
        LeftElbowShoulder = PosLeftShoulder - PosLeftElbow;
        LeftElbowJointAngle = Vector3.Angle(RightElbowWrist, RightElbowShoulder);

        LeftShoulderElbow = PosLeftElbow - PosLeftShoulder;
        LeftShoulderTrunk = PosLeftClavical - PosLeftShoulder;
        LeftShoulderJointAngle = Vector3.Angle(LeftShoulderElbow, LeftShoulderTrunk);

        AbdomenChest = PosChest - PosAbdomen;
        AbdomenHips = PosHip - PosAbdomen;
        TrunkJointAngle = Vector3.Angle(AbdomenChest,AbdomenHips);     
    }

}
