using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public static int difficulty = 0;
    public static int armSide = 0;
    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    
    public void R()
    {
        armSide = 1;
    }

    public void L()
    {
        armSide = 0;
    }

    public void S1()
    {
        difficulty = 0;
    }

    public void S2()
    {
        difficulty = 1;
    }

    public void S3()
    {
        difficulty = 2;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
