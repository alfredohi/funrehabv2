
using UnityEngine;
using System.Collections;

public enum Side {
    Left,
    Right
};

public class setArmMaterial : MonoBehaviour
{
    public Material material;

    public Side side = Side.Left;

    // Start is called before the first frame update
    void Start()
    {
        Debug.LogWarning("Start!");
        Debug.LogWarning(Settings.armSide);
        // if right side is selected, highligh right arm
        if (Settings.armSide == 1 && side == Side.Left)   {
            this.GetComponent<MeshRenderer>().material = material;
            Debug.LogWarning("Left side!!");
        }
        else if (side == Side.Right)  {
            Debug.LogWarning("Right side!!");
            this.GetComponent<MeshRenderer>().material = material;
        }
    }

    void Awake()
    {
        
        Debug.LogWarning("Awake!");
        Debug.LogWarning(Settings.armSide);
        // if right side is selected, highligh right arm
        if (Settings.armSide == 1 && side == Side.Left)   {
            this.GetComponent<MeshRenderer>().material = material;
            Debug.LogWarning("Left side!!");
        }
        else if (side == Side.Right)  {
            Debug.LogWarning("Right side!!");
            this.GetComponent<MeshRenderer>().material = material;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
