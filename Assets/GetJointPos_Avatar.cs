using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GetJointPos_Avatar : MonoBehaviour
{
    //Properties left hand:
    Vector3 LeftShoulderPos;
    Vector3 LeftElbowPos;
    Vector3 LeftHandPos;
  
    Vector3 LeftElbowHand;
    Vector3 LeftElbowShoulder;

    double LeftDotProduct;
    double LeftMagElbowHand;
    double LeftMagElbowShoulder;
    double LeftDenominator;
    double LeftElbowAngle;
    double TestAngle;

    //Properties right hand
    Vector3 RightShoulderPos;
    Vector3 RightElbowPos;
    Vector3 RightHandPos;

    Vector3 RightElbowHand;
    Vector3 RightElbowShoulder;

    double RightDotProduct;
    double RightMagElbowHand;
    double RightMagElbowShoulder;
    double RightDenominator;
    double RightElbowAngle;


    Vector3 ShoulderRot;
    Vector3 ElbowRot;
    Vector3 HandRot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(GameObject.Find("Avatar(Clone)"));///Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine3/Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm/Character1_LeftHand"));
        //hand = GameObject.Find("/hand.l");

        //Debug.Log("Check Clone Detection: ");// + GameObject.Find("Avatar(Clone)"));
        //Debug.Log(GameObject.Find("Avatar(Clone)"));
        //Debug.Log(GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine").transform.position.z);
        //Debug.Log("Left Hand:");
        LeftHandPos = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/" +
            "Character1_LeftShoulder/Character1_LeftArm/Character1_LeftForeArm/Character1_LeftHand").transform.position;
        //Debug.Log(LeftHandPos);

        //Debug.Log("Left Elbow:");
        LeftElbowPos = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder/Character1_LeftArm").transform.position;
        //Debug.Log(LeftElbowPos);

        //Debug.Log("Left Shoulder:");
        LeftShoulderPos = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_LeftShoulder").transform.position;
        //Debug.Log(LeftShoulderPos);

        LeftElbowHand = LeftHandPos - LeftElbowPos;
        LeftElbowShoulder = LeftShoulderPos - LeftElbowPos;

        LeftDotProduct = Vector3.Dot(LeftElbowHand, LeftElbowShoulder);
        LeftMagElbowHand = Vector3.Magnitude(LeftElbowHand);
        LeftMagElbowShoulder = Vector3.Magnitude(LeftElbowShoulder);
        LeftDenominator = LeftMagElbowHand * LeftMagElbowShoulder;
        LeftElbowAngle = (180 / Math.PI) * Math.Acos(LeftDotProduct / LeftDenominator);
        //Debug.Log("Angle left Elbow in degree: " + LeftElbowAngle);
        //Debug.Log(LeftElbowAngle);


        //Debug.Log("Right Hand:");
        RightHandPos = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/" +
            "Character1_RightShoulder/Character1_RightArm/Character1_RightForeArm/Character1_RightHand").transform.position;
        //Debug.Log*RightHandPos.x);

        //Debug.Log("Right Elbow:");
        RightElbowPos = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder/Character1_RightArm").transform.position;
        //Debug.Log(RightElbowPos.x);

        //Debug.Log("Right Shoulder:");
        RightShoulderPos = GameObject.Find("Avatar(Clone)/Character1_Reference/Character1_Hips/Character1_Spine/Character1_Spine1/Character1_Spine2/Character1_RightShoulder").transform.position;
        //Debug.Log(RightShoulderPos.x);

        RightElbowHand = RightHandPos - RightElbowPos;
        RightElbowShoulder = RightShoulderPos - RightElbowPos;
        //Debug.Log("Left Vector Elbow-Shoulder: " +LeftElbowShoulder);
        //Debug.Log("Left Vector Elbow-Hand: " + LeftElbowHand);
        TestAngle = Vector3.Angle(LeftElbowHand, LeftElbowShoulder);


        RightDotProduct = Vector3.Dot(RightElbowHand, RightElbowShoulder);
        RightMagElbowHand = Vector3.Magnitude(RightElbowHand);
        RightMagElbowShoulder = Vector3.Magnitude(RightElbowShoulder);
        RightDenominator = RightMagElbowHand * RightMagElbowShoulder;
        RightElbowAngle = Vector3.Angle(RightElbowHand, RightElbowShoulder);//(180 / Math.PI) * Math.Acos(RightDotProduct / RightDenominator);
        //Debug.Log("Angle right Elbow in degree: " + RightElbowAngle);
        //Debug.Log(RightElbowAngle);

    }
}
